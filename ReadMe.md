# Boomerang for Rijndael

## Installation

Using the installation script:

```shell
curl -L https://gitlab.inria.fr/lrouquet/boomerang-skinny/-/raw/master/full_installation.sh | bash
```

### Requirements

[MiniZinc](https://www.minizinc.org/)
```shell
curl -L https://github.com/MiniZinc/MiniZincIDE/releases/download/2.7.0/MiniZincIDE-2.7.0-bundle-linux-x86_64.tgz -o MiniZincIDE.tgz
tar -zxvf MiniZincIDE.tgz
```
[SDK Man](https://sdkman.io/)
```shell
curl -s "https://get.sdkman.io" | bash
```
[Picat](http://picat-lang.org/)
```shell
curl -L http://picat-lang.org/download/picat333_linux64.tar.gz -o Picat.tar.gz
tar -xvf Picat.tar.gz
cd Picat/lib
curl -L http://picat-lang.org/flatzinc/fzn_picat_sat.pi -o fzn_picat_sat.pi
curl -L http://picat-lang.org/flatzinc/fzn_parser.pi -o fzn_parser.pi
curl -L http://picat-lang.org/flatzinc/fzn_tokenizer.pi -o fzn_tokenizer.pi
sed -i "s/    writeln(';'),/    println(';'),/" fzn_picat_sat.pi
```

```shell
#!/usr/bin/env bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
git clone https://gitlab.inria.fr/lrouquet/boomerang-skinny boomerang-rijndael
cd boomerang-rijndael
mkdir modules
cd modules
  git clone git@github.com:rloic/PhD-Core.git phd-core
  cd phd-core
    git checkout 1b1229125f2fcc1fa6b02edfbaf5d10c1a3f0423
  cd ..
  git clone https://github.com/rloic/PhD-Infrastructure phd-infrastructure
  cd phd-infrastructure
    git checkout fd1e854e04ff401e5e50a6d1f24bdfb35dec90a6
  cd ..
cd ..
sdk install java 11.0.18-tem
sdk use java 11.0.18-tem
./gradlew build
```

## Usage

### Step-1

```shell
java -cp build/libs/boomerang-1.0-SNAPSHOT.jar com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step1.OptimizeKt \
MiniZinc=MiniZincIDE-2.7.0-bundle-linux-x86_64/bin/minizinc \
Picat=Picat/picat \
PicatFzn=Picat/lib/fzn_picat_sat.pi \
Nr=3 TextSize=128 KeySize=128
```
### Step-2

```shell
java -cp build/libs/boomerang-1.0-SNAPSHOT.jar com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step2.OptimizeKt  \
MiniZinc=MiniZincIDE-2.7.0-bundle-linux-x86_64/bin/minizinc \
Picat=Picat/picat \
PicatFzn=Picat/lib/fzn_picat_sat.pi \
Nr=3 TextSize=128 KeySize=128
```

## Sources

### Step-1 models

Step-1 models are written in MiniZinc and are placed in `sat/rijndael` folder. The model is separated in several files.

`Main.mzn` contains the core model. It includes attack complexity and constraints that are common to both single and related-key model.
`RelatedKey.mzn` contains the model part which is specific to related-key attacks.
`SingleKey.mzn` contains the model part which is specific to single-key attacks.

#### Application entry points

The application entry points to solve Step-1 models are:

- `com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step1.EnumerateKt` (Kotlin)
- `com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step1.OptimizeKt` (Kotlin)
- `com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step1.Optimize_iterKt` (Kotlin)

`step1.OptimizeKt` search for the optimal Step-1 solution
`step1.EnumerateKt` enumerates all the solutions that matches the given objective
`step1.Optimize_iterKt` search for the optimal Step-1 solution incrementally (from Nr=3 to Nr=14).

### Step-2 models


The application entry points to solve Step-1 models is:

- `com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step2.OptimizeKt` (Kotlin)
