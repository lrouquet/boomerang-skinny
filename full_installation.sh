#!/usr/bin/env bash

# Install SDK
curl -s "https://get.sdkman.io" | bash

# Project installation
source "$HOME/.sdkman/bin/sdkman-init.sh"
git clone https://gitlab.inria.fr/lrouquet/boomerang-skinny boomerang-rijndael
cd boomerang-rijndael
mkdir modules
cd modules
  git clone https://github.com/rloic/PhD-Core.git phd-core
  cd phd-core
    git checkout 1b1229125f2fcc1fa6b02edfbaf5d10c1a3f0423
  cd ..
  git clone https://github.com/rloic/PhD-Infrastructure phd-infrastructure
  cd phd-infrastructure
    git checkout fd1e854e04ff401e5e50a6d1f24bdfb35dec90a6
  cd ..
cd ..

# Install MiniZinc
curl -L https://github.com/MiniZinc/MiniZincIDE/releases/download/2.7.0/MiniZincIDE-2.7.0-bundle-linux-x86_64.tgz -o MiniZincIDE.tgz
tar -zxvf MiniZincIDE.tgz

# Install Picat
curl -L http://picat-lang.org/download/picat333_linux64.tar.gz -o Picat.tar.gz
tar -xvf Picat.tar.gz
cd Picat
  cd lib
  curl -L http://picat-lang.org/flatzinc/fzn_picat_sat.pi -o fzn_picat_sat.pi
  curl -L http://picat-lang.org/flatzinc/fzn_parser.pi -o fzn_parser.pi
  curl -L http://picat-lang.org/flatzinc/fzn_tokenizer.pi -o fzn_tokenizer.pi
  sed -i "s/    writeln(';'),/    println(';'),/" fzn_picat_sat.pi
  cd ..
cd ..

# Compilation
sdk install java 11.0.18-tem
sdk use java 11.0.18-tem
./gradlew build
