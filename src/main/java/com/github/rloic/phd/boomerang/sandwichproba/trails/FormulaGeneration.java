package com.github.rloic.phd.boomerang.sandwichproba.trails;

import com.github.rloic.phd.core.cryptography.attacks.boomerang.sboxtransition.SPNEBCTTransition;
import com.github.rloic.phd.boomerang.sandwichproba.sboxtransition.Formula;
import com.github.rloic.phd.core.cryptography.attacks.boomerang.sboxtransition.SPNSboxTransition;
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.Variable;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.stream.Collectors;


/**
 * This class does the computation to generate a formula helping to compute the probability of the boomerang
 *
 * @author Mathieu Vavrille
 */
public class FormulaGeneration<Cipher> {
  private final int nbRounds;
  private final UpperTrail<Cipher> upperTrail;
  private final LowerTrail<Cipher> lowerTrail;

  public FormulaGeneration(
          int nbRounds,
          UpperTrail<Cipher> upperTrail,
          LowerTrail<Cipher> lowerTrail
  ) {
    this.nbRounds = nbRounds;
    this.upperTrail = upperTrail;
    this.lowerTrail = lowerTrail;
  }

  /**
   * Get all the intersting variables, the ones that should appear in the formula
   * @param zeroVariables a set of variables equal to zero
   */
  private Set<Variable> getInterestingVariables(final Set<Variable> zeroVariables) {
    final Set<Variable> interestingVariables = new HashSet<Variable>();
    for (int round = 0; round < nbRounds; round ++) {
      for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
          if (!zeroVariables.contains(lowerTrail.getSboxState(round, i, j))) {
            interestingVariables.addAll(upperTrail.getVariablesRequiredBy(upperTrail.getSboxState(round, i, j)));
          }
          if (!zeroVariables.contains(upperTrail.getSboxState(round, i, j))) {
            interestingVariables.addAll(lowerTrail.getVariablesRequiredBy(lowerTrail.getSboxState(round, i, j)));
          }
        }
      }
    }
    return interestingVariables;
  }

  /**
   * @return a formula to compute the probability of the boomerang
   */
  public Formula getFormula() {
    final Set<Variable> zeroVariables = new HashSet<Variable>(upperTrail.getZeroVariables());
    zeroVariables.addAll(lowerTrail.getZeroVariables());
    final Set<Variable> interestingVariables = getInterestingVariables(zeroVariables);
    
    List<SPNSboxTransition> formula = new ArrayList<SPNSboxTransition>();
    for (int round = 0; round < nbRounds; round ++) {
      for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
          formula.add(SPNEBCTTransition.transitionOf(upperTrail.getLinearExpressionBeforeSbox(upperTrail.getSboxState(round, i, j)),
                                                  (interestingVariables.contains(upperTrail.getSboxState(round, i, j))) ? upperTrail.getSboxExpr(round, i, j) : null,
                                                  (interestingVariables.contains(lowerTrail.getSboxState(round, i, j))) ? lowerTrail.getSboxExpr(round, i, j) : null,
                                                  lowerTrail.getLinearExpressionAfterSbox(lowerTrail.getSboxState(round, i, j))));
        }
      }
    }
    return new Formula(formula.stream()
                       .filter(SPNSboxTransition::isInteresting)
                       .collect(Collectors.toList()));
  }
  
}
