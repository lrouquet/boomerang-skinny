package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.singlekey.step2

import com.github.rloic.phd.core.arrays.*
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isBCT
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isDDT2Lower
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isDDT2Upper
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isDDTLower
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isDDTupper
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isEBCT
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isLBCT
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isUBCT
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangTable
import com.github.rloic.phd.core.cryptography.attacks.boomerang.util.Bounds
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.Rijndael
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.Rijndael.Companion.XOR_TUPLES
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.*
import com.github.rloic.phd.core.error.panic
import com.github.rloic.phd.core.utils.Into
import org.chocosolver.solver.IModel
import org.chocosolver.solver.Model
import org.chocosolver.solver.constraints.extension.Tuples
import org.chocosolver.solver.search.strategy.Search.lastConflict
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin
import org.chocosolver.solver.search.strategy.selectors.variables.DomOverWDeg
import org.chocosolver.solver.search.strategy.selectors.variables.DomOverWDegRef
import org.chocosolver.solver.variables.IntVar


@Suppress("NonAsciiCharacters")
class RijndaelSKStep2(
    private val step1: EnumerateRijndaelBoomerangSkStep1Solution,
    bestBound: Int,
    val model: Model = Model()
) : IModel by model, Into<OptimizeSkStep2Solution> {

    val FREE = model.intVar("FREE", 256)
    val ZERO = model.intVar("ZERO", 0)

    val Nr = step1.config.Nr
    val Nb = step1.config.Nb

    val δXupper: Tensor3<IntVar>
    val δSXupper: Tensor3<IntVar>
    val δYupper: Tensor3<IntVar>
    val δZupper: Tensor3<IntVar>

    val δXlower: Tensor3<IntVar>
    val δSXlower: Tensor3<IntVar>
    val δYlower: Tensor3<IntVar>
    val δZlower: Tensor3<IntVar>

    val proba: Tensor3<IntVar?>
    val activeTable = Tensor3(Nr, 4, Nb) { _, _, _ -> BoomerangTable.None }

    val objective: IntVar

    init {
        δXupper = byteVars("δX↑", step1.X, TrailVar::upper, BoomerangSbVar::free)
        δSXupper = byteVars("δSX↑", step1.X, TrailVar::upper, BoomerangSbVar::freeS)

        δXlower = byteVars("δX↓", step1.X, TrailVar::lower, BoomerangSbVar::free)
        δSXlower = byteVars("δSX↓", step1.X, TrailVar::lower, BoomerangSbVar::freeS)

        proba = tensor3OfNulls(Nr, 4, Nb)

        var objectiveBounds = Bounds(0, 0)

        // SubCell
        for (i in 0 until Nr) {
            for (j in 0..3) {
                for (k in 0 until Nb) {
                    val ΔXupper = step1.X[i, j, k].Δ.upper
                    val freeXupper = step1.X[i, j, k].free.upper
                    val freeSBupper = step1.X[i, j, k].freeS.upper
                    val ΔXlower = step1.X[i, j, k].Δ.lower
                    val freeXlower = step1.X[i, j, k].free.lower
                    val freeSBlower = step1.X[i, j, k].freeS.lower

                    if (isDDTupper(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        proba[i, j, k] = intVar(Rijndael.SBOX_TABLES.relationDDTBounds)
                        model.table(
                            arrayOf(δXupper[i, j, k], δSXupper[i, j, k], proba[i, j, k]!!),
                            Rijndael.SBOX_TABLES.relationDDT
                        ).post()
                        if (i in 1 until Nr - 1) objectiveBounds += Rijndael.SBOX_TABLES.relationDDTBounds
                        activeTable[i, j, k] = BoomerangTable.DDT
                    }

                    if (isDDTLower(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        proba[i, j, k] = intVar(Rijndael.SBOX_TABLES.relationDDTBounds)
                        model.table(
                            arrayOf(δXlower[i, j, k], δSXlower[i, j, k], proba[i, j, k]!!),
                            Rijndael.SBOX_TABLES.relationDDT
                        ).post()
                        if (i in 1 until Nr - 1) objectiveBounds += Rijndael.SBOX_TABLES.relationDDTBounds
                        activeTable[i, j, k] = BoomerangTable.DDT
                    }

                    if (isDDT2Upper(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        proba[i, j, k] = intVar(Rijndael.SBOX_TABLES.relationDDT2Bounds)
                        model.table(
                            arrayOf(δXupper[i, j, k], δSXupper[i, j, k], proba[i, j, k]!!),
                            Rijndael.SBOX_TABLES.relationDDT2
                        ).post()
                        if (i in 1 until Nr - 1) objectiveBounds += Rijndael.SBOX_TABLES.relationDDT2Bounds
                        activeTable[i, j, k] = BoomerangTable.DDT2
                    }

                    if (isDDT2Lower(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        proba[i, j, k] = intVar(Rijndael.SBOX_TABLES.relationDDT2Bounds)
                        model.table(
                            arrayOf(δXlower[i, j, k], δSXlower[i, j, k], proba[i, j, k]!!),
                            Rijndael.SBOX_TABLES.relationDDT2
                        ).post()
                        if (i in 1 until Nr - 1) objectiveBounds += Rijndael.SBOX_TABLES.relationDDT2Bounds
                        activeTable[i, j, k] = BoomerangTable.DDT2
                    }

                    if (isBCT(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        proba[i, j, k] = intVar(Rijndael.SBOX_TABLES.relationBCTBounds)
                        model.table(
                            arrayOf(δXupper[i, j, k], δSXlower[i, j, k], proba[i, j, k]!!),
                            Rijndael.SBOX_TABLES.relationBCT
                        ).post()
                        if (i in 1 until Nr - 1) objectiveBounds += Rijndael.SBOX_TABLES.relationBCTBounds
                        activeTable[i, j, k] = BoomerangTable.BCT
                    }

                    if (isUBCT(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        proba[i, j, k] = intVar(Rijndael.SBOX_TABLES.relationUBCTBounds)
                        model.table(
                            arrayOf(δXupper[i, j, k], δSXupper[i, j, k], δSXlower[i, j, k], proba[i, j, k]!!),
                            Rijndael.SBOX_TABLES.relationUBCT
                        ).post()
                        if (i in 1 until Nr - 1) objectiveBounds += Rijndael.SBOX_TABLES.relationUBCTBounds
                        activeTable[i, j, k] = BoomerangTable.UBCT
                    }

                    if (isLBCT(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        proba[i, j, k] = intVar(Rijndael.SBOX_TABLES.relationLBCTBounds)
                        model.table(
                            arrayOf(δXupper[i, j, k], δXlower[i, j, k], δSXlower[i, j, k], proba[i, j, k]!!),
                            Rijndael.SBOX_TABLES.relationLBCT
                        ).post()
                        if (i in 1 until Nr - 1) objectiveBounds += Rijndael.SBOX_TABLES.relationLBCTBounds
                        activeTable[i, j, k] = BoomerangTable.LBCT
                    }

                    if (isEBCT(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        proba[i, j, k] = intVar(Rijndael.SBOX_TABLES.relationEBCTBounds)
                        model.table(
                            arrayOf(δXupper[i, j, k], δSXupper[i, j, k], δXlower[i, j, k], δSXlower[i, j, k], proba[i, j, k]!!),
                            Rijndael.SBOX_TABLES.relationEBCT
                        ).post()
                        if (i in 1 until Nr - 1) objectiveBounds += Rijndael.SBOX_TABLES.relationEBCTBounds
                        activeTable[i, j, k] = BoomerangTable.EBCT
                    }
                }
            }
        }

        // ShiftRows
        δYupper = Tensor3(Nr, 4, Nb) { i, j, k -> δSXupper[i, j, (k + step1.config.shift[j]) % Nb] }
        δYlower = Tensor3(Nr, 4, Nb) { i, j, k -> δSXlower[i, j, (k + step1.config.shift[j]) % Nb] }

        // ARK
        δZupper = Tensor3(Nr - 1, 4, Nb) { i, j, k -> δXupper[i + 1, j, k] }
        δZlower = Tensor3(Nr - 1, 4 , Nb) { i, j, k -> δXlower[i + 1, j, k] }

        // MixColumn
        for (i in 0 until Nr - 1) {
            mixColumn(δYupper[i], δZupper[i])
            mixColumn(δYlower[i], δZlower[i])
        }

        if (bestBound != Int.MAX_VALUE && bestBound < objectiveBounds.max) {
            if (bestBound <= objectiveBounds.min) {
                // If best bound is lower than min probabilities
                // post an unsat constraint
                arithm(boolVar(false), "=", 1).post()
            } else {
                objectiveBounds = objectiveBounds.copy(max = bestBound - 1)
            }
        }
        objective = intVar(objectiveBounds)
        model.setObjective(Model.MINIMIZE, objective)

        val middleVars = mutableListOf<IntVar>()
        val ddtVars = mutableListOf<IntVar>()

        for (i in 0 until Nr) {
            for (j in 0..3) {
                for (k in 0 until Nb) {
                    if (δXupper[i, j, k] != FREE && δSXlower[i, j, k] != FREE) {
                        middleVars += δXupper[i, j, k]
                        middleVars += δSXlower[i, j, k]
                        if (δSXupper[i, j, k] != FREE) {
                            middleVars += δSXupper[i, j, k]
                        }
                        if (δXlower[i, j, k] != FREE) {
                            middleVars += δXlower[i, j, k]
                        }
                    } else {
                        // DDTvariables
                        if (δXupper[i, j, k] != FREE)
                            ddtVars.add(δXupper[i, j, k]);
                        if (δSXupper[i, j, k] != FREE)
                            ddtVars.add(δSXupper[i, j, k]);
                        if (δXlower[i, j, k] != FREE)
                            ddtVars.add(δXlower[i, j, k]);
                        if (δSXlower[i, j, k] != FREE)
                            ddtVars.add(δSXlower[i, j, k]);
                    }
                }
            }
        }

        check(FREE.propagators.isEmpty() || FREE.propagators.all { it == null })
        check(ZERO.propagators.isEmpty() || ZERO.propagators.all { it == null })

        middleVars.removeAll { it == ZERO || it == ZERO }
        middleVars.removeAll { it == ZERO || it == FREE }

        println((activeTable.deepFlatten()).groupBy { it }.map { (k, v) -> k to v.size })

        val probaVars = mutableListOf<IntVar>()
        for (i in 1 until Nr - 1) {
            for (j in 0..3) {
                for (k in 0 until Nb) {
                    proba[i, j, k]?.let { probaVars += it }
                }
            }

        }

        model.sum(probaVars.toTypedArray(), "=", objective).post()

        val solver = model.solver
        solver.setSearch(
            lastConflict(
                DomOverWDegRef(
                    (middleVars + probaVars + ddtVars).toTypedArray() + model.retrieveIntVars(true),
                    0L,
                    IntDomainMin()
                )
            )
        )
    }

    private fun intVar(bounds: Bounds) = intVar(bounds.min, bounds.max)

    private fun byteVars(
        name: String,
        abstractState: Tensor3<BoomerangSbVar>,
        trail: (TrailVar) -> Int,
        freeSelector: (BoomerangSbVar) -> TrailVar
    ) =
        Tensor3(abstractState.dim1, abstractState.dim2, abstractState.dim3) { i, j, k ->
            if (trail(freeSelector(abstractState[i, j, k])) == 0) {
                if (trail(abstractState[i, j, k].Δ) == 1) {
                    intVar("$name[$i, $j, $k]", 1, 255)
                } else {
                    ZERO
                }
            } else {
                FREE
            }
        }

    private fun mixColumn(δY: Matrix<IntVar>, δZ: Matrix<IntVar>) {
        val δY2 = Matrix(4, Nb) { j, k -> galoisFieldMul(δY[j, k], 2) }
        val δY3 = Matrix(4, Nb) { j, k -> galoisFieldMul(δY[j, k], 3) }

        for (k in 0 until Nb) {
            postXor(δZ[0, k], xor(δY2[0, k], δY3[1, k]), xor(δY[2, k], δY[3, k]))
            postXor(δZ[1, k], xor(δY[0, k], δY2[1, k]), xor(δY3[2, k], δY[3, k]))
            postXor(δZ[2, k], xor(δY[0, k], δY[1, k]), xor(δY2[2, k], δY3[3, k]))
            postXor(δZ[3, k], xor(δY3[0, k], δY[1, k]), xor(δY[2, k], δY2[3, k]))
        }

        val δZ09 = Matrix(4, Nb) { j, k -> galoisFieldMul(δZ[j, k], 9) }
        val δZ11 = Matrix(4, Nb) { j, k -> galoisFieldMul(δZ[j, k], 11) }
        val δZ13 = Matrix(4, Nb) { j, k -> galoisFieldMul(δZ[j, k], 13) }
        val δZ14 = Matrix(4, Nb) { j, k -> galoisFieldMul(δZ[j, k], 14) }

        for (k in 0 until Nb) {
            postXor(δY[0, k], xor(δZ14[0, k], δZ11[1, k]), xor(δZ13[2, k], δZ09[3, k]))
            postXor(δY[1, k], xor(δZ09[0, k], δZ14[1, k]), xor(δZ11[2, k], δZ13[3, k]))
            postXor(δY[2, k], xor(δZ13[0, k], δZ09[1, k]), xor(δZ14[2, k], δZ11[3, k]))
            postXor(δY[3, k], xor(δZ11[0, k], δZ13[1, k]), xor(δZ09[2, k], δZ14[3, k]))
        }
    }

    private fun galoisFieldMul(v: IntVar, k: Int): IntVar {
        if (v == FREE) {
            return FREE
        }
        if (v == ZERO) {
            return ZERO
        }

        val tuples = when (k) {
            2 -> Rijndael.mul2
            3 -> Rijndael.mul3
            9 -> Rijndael.mul9
            11 -> Rijndael.mul11
            13 -> Rijndael.mul13
            14 -> Rijndael.mul14
            else -> panic("Invalid coefficient $k in MC ")
        }

        val res = intVar("$k${v.name}", 0, 255)
        table(v, res, tuples).post()
        return res
    }

    private fun postXor(a: IntVar, b: IntVar, c: IntVar) {
        if (a == FREE || b == FREE || c == FREE) {
            return
        }

        if (a == ZERO && b == ZERO && c == ZERO) {
            return
        }
        if (a == ZERO && b == ZERO) {
            arithm(c, "=", 0).post(); return
        }
        if (b == ZERO && c == ZERO) {
            arithm(a, "=", 0).post(); return
        }
        if (c == ZERO && a == ZERO) {
            arithm(b, "=", 0).post(); return
        }
        if (a == ZERO) {
            arithm(b, "=", c).post(); return
        }
        if (b == ZERO) {
            arithm(a, "=", c).post(); return
        }
        if (c == ZERO) {
            arithm(a, "=", b).post(); return
        }
        table(arrayOf(a, b, c), XOR_TUPLES, "FC").post()
    }

    private fun Tensor3<IntVar>.values(): IntTensor3 = IntTensor3(dim1, dim2, dim3) { i, j, k -> this[i, j, k].value }
    private fun Tensor3<IntVar?>.values(): Tensor3<Int?> = Tensor3(dim1, dim2, dim3) { i, j, k -> this[i, j, k]?.value }

    private fun Matrix<IntVar>.values(): IntMatrix = IntMatrix(dim1, dim2) { i, j -> this[i, j].value }
    private fun Matrix<IntVar?>.values(): Matrix<Int?> = Matrix(dim1, dim2) { i, j -> this[i, j]?.value }

    private fun xor(lhs: IntVar, rhs: IntVar): IntVar {
        if (lhs == FREE || rhs == FREE) {
            return FREE
        }
        if (lhs == ZERO && rhs == ZERO) {
            return ZERO
        }
        if (lhs == ZERO) return rhs
        if (rhs == ZERO) return lhs

        val tmp = intVar("${lhs.name} ^ ${rhs.name}", 0, 255)
        table(arrayOf(lhs, rhs, tmp), XOR_TUPLES, "FC").post()
        return tmp
    }

    override fun into(): OptimizeSkStep2Solution = OptimizeSkStep2Solution(
        step1.config, objective.value,
        δXupper.values(), δSXupper.values(), δYupper.values(), δZupper.values(),
        δXlower.values(), δSXlower.values(), δYlower.values(), δZlower.values(),
        proba.values(), activeTable
    )
}