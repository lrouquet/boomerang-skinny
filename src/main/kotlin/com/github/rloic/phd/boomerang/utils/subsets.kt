package com.github.rloic.phd.boomerang.utils

fun <T> subsets(S: List<T>, fn: (List<T>) -> Unit) {
    subsetsRec(S, S.toMutableList(), 0, 0, fn)
}

private fun <T> subsetsRec(S: List<T>, subset: MutableList<T>, pos: Int, len: Int, fn: (List<T>) -> Unit) {
    if (pos == S.size) { fn(List(len) { subset[it] }); return }
    subset[len] = S[pos]
    subsetsRec(S, subset, pos + 1, len + 1, fn)
    subsetsRec(S, subset, pos + 1, len, fn)
}

private fun <T> subsetsRec2(S: List<T>, subset: MutableList<T>, pos: Int, len: Int): List<List<T>> {
    if (pos == S.size) { return listOf(List(len) { subset[it] }) }
    subset[len] = S[pos]
    return subsetsRec2(S, subset, pos + 1, len + 1) + subsetsRec2(S, subset, pos + 1, len)
}