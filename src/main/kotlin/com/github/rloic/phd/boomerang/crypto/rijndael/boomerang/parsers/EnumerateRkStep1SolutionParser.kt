package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.parsers

import com.github.rloic.phd.core.arrays.Matrix
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.RkRijndael
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.EnumerateRijndaelBoomerangRkStep1Solution
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.BoomerangOptionalSbVar
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.TrailVar
import com.github.rloic.phd.core.mzn.MznSolution
import com.github.rloic.phd.core.mzn.MznSolutionParser
import javaextensions.util.reshape

@Suppress("NonAsciiCharacters")
class EnumerateRkStep1SolutionParser(val config: RkRijndael) :
    MznSolutionParser<EnumerateRijndaelBoomerangRkStep1Solution> {
    private val delegate = EnumerateSkStep1SolutionParser(config)
    override fun parse(serialized: MznSolution): EnumerateRijndaelBoomerangRkStep1Solution {
        val Nr = config.Nr
        val Nb = config.Nb

        val elements = serialized.content.split('\n')

        val solution = delegate.parse(serialized)

        val DWKupper = elements.getIntArray("DWKupper").reshape(4, (Nr + 1) * Nb)
        val DWKlower = elements.getIntArray("DWKlower").reshape(4, (Nr + 1) * Nb)

        val freeWKupper = elements.getIntArray("freeWKupper").reshape(4, (Nr + 1) * Nb)
        val freeWKlower = elements.getIntArray("freeWKlower").reshape(4, (Nr + 1) * Nb)

        val freeSWKupper = elements.getIntArray("freeSWKupper").reshape(4, (Nr + 1) * Nb)
        val freeSWKlower = elements.getIntArray("freeSWKlower").reshape(4, (Nr + 1) * Nb)

        val WK = Matrix(4, (Nr + 1) * Nb) { j, ik ->
            BoomerangOptionalSbVar(
                TrailVar(DWKupper[j, ik], DWKlower[j, ik]),
                TrailVar(freeWKupper[j, ik], freeWKlower[j, ik]),
                if (config.isSbColumn(ik)) TrailVar(freeSWKupper[j, ik], freeSWKlower[j, ik]) else null
            )
        }

        return EnumerateRijndaelBoomerangRkStep1Solution(config, solution.X, solution.Y, solution.Z, WK, solution.ATTACK_I, solution.ATTACK_II, solution.ATTACK_III, solution.keyProba, solution.twoT, solution.rb, solution.rf, solution.timeComplexity)
    }
}