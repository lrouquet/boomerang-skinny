package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step1

import com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.parsers.EnumerateRkStep1SolutionParser
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.RkRijndael
import com.github.rloic.phd.core.mzn.Assignment
import com.github.rloic.phd.core.mzn.MznSolution
import javaextensions.util.mapToBool
import javaextensions.util.mapToIntArray

@Suppress("NonAsciiCharacters")
class RijndaelRkPartialAssignment(private val config: RkRijndael) {

    fun extractSolution(mznSolution: MznSolution): Assignment.Partial {
        val solution = EnumerateRkStep1SolutionParser(config).parse(mznSolution)

        // Upper trail
        val flattenedΔXupper = solution.X.deepFlatten().mapToIntArray { it.Δ.upper }.mapToBool().contentToString()
        val flattenedFreeXupper = solution.X.deepFlatten().mapToIntArray { it.free.upper }.mapToBool().contentToString()
        val flattenedFreeSBupper = solution.X.deepFlatten().mapToIntArray { it.freeS.upper }.mapToBool().contentToString()

        // Upper trail Key
        val flattenedΔWKupper = solution.WK.deepFlatten().mapToIntArray { it.Δ.upper }.mapToBool().contentToString()
        val flattenedFreeWKupper = solution.WK.deepFlatten().mapToIntArray { it.free.upper }.mapToBool().contentToString()
        val flattenedFreeSWKupper = solution.WK.deepFlatten().mapToIntArray { it.freeS?.upper ?: -1 }.mapToBool().contentToString()

        // Lower trail
        val flattenedΔXlower = solution.X.deepFlatten().mapToIntArray { it.Δ.lower }.mapToBool().contentToString()
        val flattenedFreeXlower = solution.X.deepFlatten().mapToIntArray { it.free.lower }.mapToBool().contentToString()
        val flattenedFreeSBlower = solution.X.deepFlatten().mapToIntArray { it.freeS.lower }.mapToBool().contentToString()

        // Lower trail Key
        val flattenedΔWKlower = solution.WK.deepFlatten().mapToIntArray { it.Δ.lower }.mapToBool().contentToString()
        val flattenedFreeWKlower = solution.WK.deepFlatten().mapToIntArray { it.free.lower }.mapToBool().contentToString()
        val flattenedFreeSWKlower = solution.WK.deepFlatten().mapToIntArray { it.freeS?.lower ?: -1 }.mapToBool().contentToString()

        return Assignment.Partial(buildString {
            // Upper trail
            appendLine("array [ROUNDS, ROWS, BLOCK_COLS] of var bool: DXupper_%SOL% = array3d(ROUNDS, ROWS, BLOCK_COLS, $flattenedΔXupper);")
            appendLine("array [ROUNDS, ROWS, BLOCK_COLS] of var bool: freeXupper_%SOL% = array3d(ROUNDS, ROWS, BLOCK_COLS, $flattenedFreeXupper);")
            appendLine("array [ROUNDS, ROWS, BLOCK_COLS] of var bool: freeSBupper_%SOL% = array3d(ROUNDS, ROWS, BLOCK_COLS, $flattenedFreeSBupper);")
            // Upper Trail Key
            appendLine("array [ROWS, FULL_KEY_COLS] of var bool: DWKupper_%SOL% = array2d(ROWS, FULL_KEY_COLS, $flattenedΔWKupper);")
            appendLine("array [ROWS, FULL_KEY_COLS] of var bool: freeWKupper_%SOL% = array2d(ROWS, FULL_KEY_COLS, $flattenedFreeWKupper);")
            appendLine("array [ROWS, FULL_KEY_COLS] of var bool: freeSWKupper_%SOL% = array2d(ROWS, FULL_KEY_COLS, $flattenedFreeSWKupper);")
            // Lower trail
            appendLine("array [ROUNDS, ROWS, BLOCK_COLS] of var bool: DXlower_%SOL% = array3d(ROUNDS, ROWS, BLOCK_COLS, $flattenedΔXlower);")
            appendLine("array [ROUNDS, ROWS, BLOCK_COLS] of var bool: freeXlower_%SOL% = array3d(ROUNDS, ROWS, BLOCK_COLS, $flattenedFreeXlower);")
            appendLine("array [ROUNDS, ROWS, BLOCK_COLS] of var bool: freeSBlower_%SOL% = array3d(ROUNDS, ROWS, BLOCK_COLS, $flattenedFreeSBlower);")
            // Upper Trail Key
            appendLine("array [ROWS, FULL_KEY_COLS] of var bool: DWKlower_%SOL% = array2d(ROWS, FULL_KEY_COLS, $flattenedΔWKlower);")
            appendLine("array [ROWS, FULL_KEY_COLS] of var bool: freeWKlower_%SOL% = array2d(ROWS, FULL_KEY_COLS, $flattenedFreeWKlower);")
            appendLine("array [ROWS, FULL_KEY_COLS] of var bool: freeSWKlower_%SOL% = array2d(ROWS, FULL_KEY_COLS, $flattenedFreeSWKlower);")
        })
    }

}