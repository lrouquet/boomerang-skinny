package com.github.rloic.phd.boomerang.crypto.skinny

enum class Mod : Comparable<Mod> {
    SK, TK1, TK2, TK3
}