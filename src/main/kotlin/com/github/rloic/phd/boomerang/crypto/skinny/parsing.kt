package com.github.rloic.phd.boomerang.crypto.skinny

import boomerangsearch.step1.Step1Solution
import boomerangsearch.step1.Step1SolutionTweakey
import com.github.rloic.phd.core.arrays.IntTensor3

private fun int(b: Boolean) = if (b) 1 else 0

private fun toLinear(cells: String) = cells
    .substringAfter('[')
    .substringBefore(']')
    .split(',')
    .map { it == "true" }

private fun List<String>.getLinear(key: String) =
    toLinear(first { it.startsWith(key) })

private fun split(content: String) =
    content
        .split("----------")
        .dropLast(1)

fun parse(R: Int, mod: Mod) = { content: String ->
    val bestSolutions = mutableListOf<Step1Solution>()
    for (solution in split(content)) {
        val elements = solution.split('\n')

        val DXupperLinear = elements.getLinear("DXupper")
        val DXupper = IntTensor3(R, 4, 4)

        val freeXupperLinear = elements.getLinear("freeXupper")
        val freeXupper = IntTensor3(R, 4, 4)

        val freeSBupperLinear = elements.getLinear("freeSBupper")
        val freeSBupper = IntTensor3(R, 4, 4)

        val DXlowerLinear = elements.getLinear("DXlower")
        val DXlower = IntTensor3(R, 4, 4)

        val freeXlowerLinear = elements.getLinear("freeXlower")
        val freeXlower = IntTensor3(R, 4, 4)

        val freeSBlowerLinear = elements.getLinear("freeSBlower")
        val freeSBlower = IntTensor3(R, 4, 4)

        val isDDT2Linear = elements.getLinear("isDDT2")
        val isDDT2 = IntTensor3(R, 4, 4)

        val isTableLinear = elements.getLinear("isTable")
        val isTable = IntTensor3(R, 4, 4)

        var cpt = 0
        for (r in 0 until R) {
            for (j in 0..3) {
                for (k in 0..3) {
                    DXupper[r, j, k] = int(DXupperLinear[cpt])
                    freeXupper[r, j, k] = int(freeXupperLinear[cpt])
                    freeSBupper[r, j, k] = int(freeSBupperLinear[cpt])
                    DXlower[r, j, k] = int(DXlowerLinear[cpt])
                    freeXlower[r, j, k] = int(freeXlowerLinear[cpt])
                    freeSBlower[r, j, k] = int(freeSBlowerLinear[cpt])
                    isDDT2[r, j, k] = int(isDDT2Linear[cpt])
                    isTable[r, j, k] = int(isTableLinear[cpt])
                    cpt += 1
                }
            }
        }

        val objective =
            elements.firstOrNull { it.startsWith("objective") }?.substringAfter("= ")?.substringBefore(';')?.toInt()
                ?: (isTable.sum() + isDDT2.sum())

        var DTKupper: IntTensor3? = null
        var DTKlower: IntTensor3? = null

        if (mod >= Mod.TK1) {
            val DTKupperLinear = elements.getLinear("DTKupper")
            DTKupper = IntTensor3(R - 1, 2, 4)
            val DTKlowerLinear = elements.getLinear("DTKlower")
            DTKlower = IntTensor3(R - 1, 2, 4)

            cpt = 0
            for (i in 0 until R - 1) {
                for (j in 0..1) {
                    for (k in 0..3) {
                        DTKupper[i, j, k] = int(DTKupperLinear[cpt])
                        DTKlower[i, j, k] = int(DTKlowerLinear[cpt])
                        cpt += 1
                    }
                }
            }
        }

        var lanesUpper: IntArray? = null
        var lanesLower: IntArray? = null

        if (mod >= Mod.TK2) {
            val lanesUpperLinear = elements.getLinear("lanesupper")
            lanesUpper = IntArray(16) { int(lanesUpperLinear[it]) }
            val lanesLowerLinear = elements.getLinear("laneslower")
            lanesLower = IntArray(16) { int(lanesLowerLinear[it]) }
        }

        val curr = Step1Solution(
            R,
            mod,
            objective,
            DXupper.toArrays(),
            freeXupper.toArrays(),
            freeSBupper.toArrays(),
            Step1SolutionTweakey(DTKupper?.toArrays(), lanesUpper),
            DXlower.toArrays(),
            freeXlower.toArrays(),
            freeSBlower.toArrays(),
            Step1SolutionTweakey(DTKlower?.toArrays(), lanesLower),
            isTable.toArrays(),
            isDDT2.toArrays()
        )
        if (bestSolutions.isNotEmpty() && curr.objective < bestSolutions[0].objective) bestSolutions.clear()
        bestSolutions += curr
    }

    bestSolutions
}