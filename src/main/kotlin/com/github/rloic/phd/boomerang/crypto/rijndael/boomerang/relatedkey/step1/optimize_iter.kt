package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step1

import com.github.rloic.phd.core.cryptography.ciphers.rijndael.RkRijndael
import com.github.rloic.phd.core.utils.Logger
import com.github.rloic.phd.core.utils.logger
import com.github.rloic.phd.core.utils.parseArgs
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import java.io.File

fun main(_args_: Array<String>) {
    val args = parseArgs(_args_)
    logger = Logger.from(args)
    logger.addTerminal()

    args.putIfAbsent("Nr", "3")

    val minizinc = MiniZincBinary.from(args)
    val picat = PicatBinary.from(args, minizinc)
    var configuration = RkRijndael.from(args)

    val probabilities = mutableListOf<Int>()
    var searchForNextRound = true
    var ub = 0
    while (searchForNextRound) {
        searchForNextRound = false
        logger.log("Solving for %s", configuration)
        val start = System.currentTimeMillis()
        val solution = optimizeRk(picat, configuration, ub)
        if (solution != null) {
            // Latex.preview(::RkStep1SolutionLatexPresenter, solution)
            logger.log("Best probability: 2^{-%d}", solution.objStep1)
            if (solution.objStep1 < configuration.keySize.nbBits) {
                probabilities += solution.objStep1
                ub = solution.objStep1
                searchForNextRound = configuration.Nr + 1 <= configuration.MAX_NR
                configuration = RkRijndael(configuration.Nr + 1, configuration.textSize, configuration.keySize)
            }
        } else {
            logger.log("UNSAT")
        }
        val duration = (System.currentTimeMillis() - start) / 1000.0
        logger.log("Computation time: %.2fs", duration)
    }

    logger.log("Probabilities: ")
    logger.log(probabilities.joinToString(", "))
}